import {faker} from '@faker-js/faker';
// @mui
import {useTheme} from '@mui/material/styles';
import {Grid, Container, Typography} from '@mui/material';
// components
import {useEffect, useState} from "react";
import Page from '../components/Page';
import Iconify from '../components/Iconify';
// sections
import {
    AppTasks,
    AppNewsUpdate,
    AppOrderTimeline,
    AppCurrentVisits,
    AppWebsiteVisits,
    AppTrafficBySite,
    AppWidgetSummary,
    AppCurrentSubject,
    AppConversionRates,
} from '../sections/@dashboard/app';
import {getGameById} from "../api";

// ----------------------------------------------------------------------

export default function DashboardApp() {
    const theme = useTheme();

    // const [game, setGame] = useState(null);
    //
    // useEffect(() => {
    //     getGameById(1).then(response => setGame(response.data));
    // }, []);
    //
    // const currentUser = JSON.parse(localStorage.getItem("user"));

    return (
        <Page title="Dashboard">
            <Container maxWidth="xl">
                <Typography variant="h4" sx={{mb: 5}}>
                    Hi, Welcome back
                    {/*{!!currentUser && (currentUser.firstname + " " + currentUser.lastname)}*/}
                    {/*{!!game && JSON.stringify(game)}*/}
                </Typography>

                <Grid container spacing={3}>
                    <Grid item xs={12} sm={6} md={3}>
                        <AppWidgetSummary title="Games Added To Profiles" total={1024} icon={'icon-park:game-two'}/>
                    </Grid>

                    <Grid item xs={12} sm={6} md={3}>
                        <AppWidgetSummary title="Your Games" total={356} color="warning"
                                          icon={'icon-park-solid:game-three'}/>
                    </Grid>

                    <Grid item xs={12} sm={6} md={3}>
                        <AppWidgetSummary title="Followers" total={105} color="info"
                                          icon={'mingcute:user-follow-fill'}/>
                    </Grid>

                    <Grid item xs={12} sm={6} md={3}>
                        <AppWidgetSummary title="Followings" total={236} color="error"
                                          icon={'mingcute:user-follow-line'}/>
                    </Grid>


                    <Grid item xs={12} md={6} lg={4}>
                        <AppCurrentVisits
                            title="Popular Platforms"
                            chartData={[
                                {label: 'PSN', value: 4500},
                                {label: 'Xbox Live', value: 1500},
                                {label: 'Steam', value: 6000},
                                {label: 'Epic', value: 2300},
                                {label: 'Nintendo', value: 1000},
                            ]}
                        />
                    </Grid>

                    <Grid item xs={12} md={6} lg={8}>
                        <AppConversionRates
                            title="Popular Games Today (Peak Player) "
                            subheader="(+43%) than last year"
                            chartData={[
                                {label: 'Counter-Strike: Global Offensive', value: 989650},
                                {label: 'Dota 2', value: 670534},
                                {label: 'PUBG: BATTLEGROUNDS', value: 397738},
                                {label: 'Apex Legends', value: 337616},
                                {label: 'Grand Theft Auto V', value: 144206},
                                {label: 'Cyberpunk 2077', value: 88535},
                                {label: 'Destiny 2', value: 84494},
                            ]}
                        />
                    </Grid>


                    {/*<Grid item xs={12} md={6} lg={8}>*/}
                    {/*    <AppNewsUpdate*/}
                    {/*        title="News Update"*/}
                    {/*        list={[...Array(5)].map((_, index) => ({*/}
                    {/*            id: faker.datatype.uuid(),*/}
                    {/*            title: faker.name.jobTitle(),*/}
                    {/*            description: faker.name.jobTitle(),*/}
                    {/*            image: `/static/mock-images/covers/cover_${index + 1}.jpg`,*/}
                    {/*            postedAt: faker.date.recent(),*/}
                    {/*        }))}*/}
                    {/*    />*/}
                    {/*</Grid>*/}

                    {/*<Grid item xs={12} md={6} lg={4}>*/}
                    {/*    <AppOrderTimeline*/}
                    {/*        title="Order Timeline"*/}
                    {/*        list={[...Array(5)].map((_, index) => ({*/}
                    {/*            id: faker.datatype.uuid(),*/}
                    {/*            title: [*/}
                    {/*                '1983, orders, $4220',*/}
                    {/*                '12 Invoices have been paid',*/}
                    {/*                'Order #37745 from September',*/}
                    {/*                'New order placed #XF-2356',*/}
                    {/*                'New order placed #XF-2346',*/}
                    {/*            ][index],*/}
                    {/*            type: `order${index + 1}`,*/}
                    {/*            time: faker.date.past(),*/}
                    {/*        }))}*/}
                    {/*    />*/}
                    {/*</Grid>*/}

                    {/*<Grid item xs={12} md={6} lg={4}>*/}
                    {/*    <AppTrafficBySite*/}
                    {/*        title="Traffic by Site"*/}
                    {/*        list={[*/}
                    {/*            {*/}
                    {/*                name: 'FaceBook',*/}
                    {/*                value: 323234,*/}
                    {/*                icon: <Iconify icon={'eva:facebook-fill'} color="#1877F2" width={32} height={32}/>,*/}
                    {/*            },*/}
                    {/*            {*/}
                    {/*                name: 'Google',*/}
                    {/*                value: 341212,*/}
                    {/*                icon: <Iconify icon={'eva:google-fill'} color="#DF3E30" width={32} height={32}/>,*/}
                    {/*            },*/}
                    {/*            {*/}
                    {/*                name: 'Linkedin',*/}
                    {/*                value: 411213,*/}
                    {/*                icon: <Iconify icon={'eva:linkedin-fill'} color="#006097" width={32} height={32}/>,*/}
                    {/*            },*/}
                    {/*            {*/}
                    {/*                name: 'Twitter',*/}
                    {/*                value: 443232,*/}
                    {/*                icon: <Iconify icon={'eva:twitter-fill'} color="#1C9CEA" width={32} height={32}/>,*/}
                    {/*            },*/}
                    {/*        ]}*/}
                    {/*    />*/}
                    {/*</Grid>*/}

                    {/*<Grid item xs={12} md={6} lg={8}>*/}
                    {/*    <AppTasks*/}
                    {/*        title="Tasks"*/}
                    {/*        list={[*/}
                    {/*            {id: '1', label: 'Create FireStone Logo'},*/}
                    {/*            {id: '2', label: 'Add SCSS and JS files if required'},*/}
                    {/*            {id: '3', label: 'Stakeholder Meeting'},*/}
                    {/*            {id: '4', label: 'Scoping & Estimations'},*/}
                    {/*            {id: '5', label: 'Sprint Showcase'},*/}
                    {/*        ]}*/}
                    {/*    />*/}
                    {/*</Grid>*/}
                </Grid>
            </Container>
        </Page>
    );
}
