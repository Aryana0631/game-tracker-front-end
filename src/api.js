import axios from 'axios';
// config

// ----------------------------------------------------------------------

const axiosInstance = axios.create({
    baseURL: "http://localhost",
});

axiosInstance.interceptors.response.use(
    (response) => response,
    (error) => Promise.reject(error)
);


export const login = async (email, password) => {
    const tokenResponse = await axiosInstance.post('/authentication_token', {
        email,
        password,
    });
    const {token} = tokenResponse.data;

    axiosInstance.defaults.headers.common.Authorization = `Bearer ${token}`;
    localStorage.setItem('token', token);


    const userResponse = await axiosInstance.get('/user/get_current_user');

    const user = userResponse.data;

    localStorage.setItem('user', user);
};


export const register = async (email, password, firstname, lastname, username) => {
    const userResponse = await axiosInstance.post('/register', {
        email,
        password,
        firstname,
        lastname,
        username
    });

    return true;

};

export const getGameById = async (gameId) => {
    return axiosInstance.get(`/api/games/${gameId}`);
};
